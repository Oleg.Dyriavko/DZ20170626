public class Main {


    private static final String[] CABBAGE = {
            "         CABBAGE",
            "         .-~~~~-.",
            "        /( ( ' \\ \\ ",
            "       | ( )   )  |",
            "       \\ ) ' }  / /",
            "       (` \\ , /  ~)",
            "        `-.`\\/_.-'",
            "          `\"\""
    };
    private static final String[] GOAT = {
                     "             GOAT\n" +
                     "             / /\n" +
                    "          (\\/_//`)\n" +
                    "           /   '/\n" +
                    "          0  0   \\\n" +
                    "         /   __/   \\\n" +
                    "        `-./)  |     ~^~^~^~^~^~^~^~\\~.\n" +
                    "           (   /                     \\_}\n" +
                    "              |               /      |\n" +
                    "               \\/ ,/           \\    |\n" +
                    "               / /~~|~|~~~~~~|~|\\   |\n" +
                    "              / (   | |      | |  \\  \\\n" +
                    "             /,_)  /__)     /__)  /,_/"
    };
    private static final String[] WOLF = {
"\n" +
"                       WOLF\n" +
"                       .",
"                     // V\\",
"                   // `  /",
"                 <<      |",
"                  /      |",
"                 /       |",
"               /        |",
"             /          /",
"            (      ) | |",
"   ________|    _/_  | |",
"< __________(______)(__)",

    };

    private static final String[] MAN = {
            "\n" +
                    "                FARMER\n" +
                    "                ,sss.  \n" +
                    "                $^,^$\n" +
                    "               _/$$$\\_\n" +
                    "         |   /'  ?$?  `.\n" +
                    "         ;,-' /\\ ,, /. |\n" +
                    "         '-./' ;    ;: |\n" +
                    "         |     |`  '|`,;\n" +
                    "~~~~~~~~~~~~~~~~~~~~\n",
    };


    private static final String[] MAN_and_GOAT_right = {
            "       Farmer and goat sail to the right bank  \n" +
                    "\n" +
                    "                               / /\n" +
                    "         ,sss.             (\\/_//`)\n" +
                    "         $^,^$              /   '/   \n" +
                    "  |     _/$$$\\_     +      0  0   \\                          ==== >>\n" +
                    "  |   /'  ?$?  `.         /   __/   \\\n" +
                    "  ;,-' /\\ ,, /. |        `-./)  |     ~^~^~^~^~^~^~^~\\~.\n" +
                    "  '-./' ;    ;: |            (   /                     \\_}\n" +
                    "  |     |`  '|`,;               |               /      |\n" +
                    "  |     |`  '|`,;                \\/ ,/           \\    |\n",
                    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

    };

    private static final String[] MAN_left = {

            "        Farmer returns to the left bank\n" +
                    "\n" +
                    "         ,sss.  \n" +
                    "         $^,^$\n" +
                    "        _/$$$\\_\n" +
                    "  |   /'  ?$?  `.                                         << ====\n" +
                    "  ;,-' /\\ ,, /. |\n" +
                    "  '-./' ;    ;: |\n" +
                    "  |     |`  '|`,;\n" +
                    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",
    };

    private static final String[] MAN_and_WOLF_right = {
            "       Farmer and wolf sail to the right bank  \n" +
                    "\n" +
                    "                                      .\n" +
                    "         ,sss.                     // V\\\n" +
                    "         $^,^$                   // `  /\n" +
                    "  |     _/$$$\\_     +         <<      |                       ==== >>\n" +
                    "  |   /'  ?$?  `.              /      |\n" +
                    "  ;,-' /\\ ,, /. |             /       |\n" +
                    "  '-./' ;    ;: |            /        |\n" +
                    "  |     |`  '|`,;          /          /\n" +
                    "  |     |`  '|`,;         (      ) | |\n",
            "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

    };




    private static final String[] MAN_and_GOAT_left = {
            "       Farmer and goat return to the left bank  \n" +
                    "\n" +
                    "                               / /\n" +
                    "         ,sss.             (\\/_//`)\n" +
                    "         $^,^$              /   '/   \n" +
                    "  |     _/$$$\\_     +      0  0   \\                         << ====\n" +
                    "  |   /'  ?$?  `.         /   __/   \\\n" +
                    "  ;,-' /\\ ,, /. |        `-./)  |     ~^~^~^~^~^~^~^~\\~.\n" +
                    "  '-./' ;    ;: |            (   /                     \\_}\n" +
                    "  |     |`  '|`,;               |               /      |\n" +
                    "  |     |`  '|`,;                \\/ ,/           \\    |\n",
            "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",

    };

    private static final String[] MAN_and_CABBAGE_right = {

            "     Farmer and cabbage float on the right bank\n" +
                    "\n" +
                    "         ,sss.            .-~~~~-.\n" +
                    "         $^,^$           /( ( ' \\ \\ \n" +
                    "  |     _/$$$\\_    +    | ( )   )  |                          ==== >>\n" +
                    "  |   /'  ?$?  `.       \\ ) ' }  / /\n" +
                    "  ;,-' /\\ ,, /. |       (` \\ , /  ~)\n" +
                    "  '-./' ;    ;: |        `-.`\\/_.-'\n" +
                    "  |     |`  '|`,;           `\"\"\n" +
                    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n",
    };






    public static void main(String[] args) {
        print(CABBAGE);
        print(GOAT);
        print(WOLF);
        print(MAN);

        System.out.println("Task: The farmer must safely transport the goat, wolf and cabbage to the right bank. \n");

        System.out.println("                  STEP 1");
        print(MAN_and_GOAT_right);

        System.out.println("                  STEP 2");
        print(MAN_left);

        System.out.println("                  STEP 3");
        print(MAN_and_WOLF_right);

        System.out.println("                  STEP 4");
        print(MAN_and_GOAT_left);

        System.out.println("                  STEP 5");
        print(MAN_and_CABBAGE_right);

        System.out.println("                  STEP 6");
        print(MAN_left);

        System.out.println("                  STEP 7");
        print(MAN_and_GOAT_right);

        System.out.println("           MISSION ACCOMPLISHED !!!");

    }


    public static void print(String[] strings){

        for (int i = 0; i < strings.length; i++) {
            System.out.println(strings[i]);
        }

    }

}

